package com.kryviak.arraystask;

import java.util.Arrays;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        // Task A 1
        int[] arr1 = getRandomArray();
        int[] arr2 = getRandomArray();
        int[] arr3 = addTwoArrays(arr1, arr2);

        //Task A2
        int[] arr4 = getRandomArray().clone();

        //Task B
        removeDuplications(arr4);

        //Task C
        printArrWithoutDuplications(new int[]{1,1,1,1,1,1});
    }

    private static int[] getRandomArray() {
        Random random = new Random();
        int[] arr = new int[100];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = random.nextInt(100);
        }
        return arr;
    }

    private static int[] addTwoArrays(int[] arr1, int[] arr2) {
        int len = arr1.length + arr2.length;
        int[] result = new int[len];
        int tempIndex = 0;
        for (int i = 0; i < arr1.length; i++) {
            result[tempIndex] = arr1[i];
            tempIndex++;
        }
        for (int j = 0; j < arr2.length; j++) {
            result[tempIndex] = arr2[j];
            tempIndex++;
        }
        return result;
    }

    private static int[] removeDuplications(int[] arr) {
        int[] tempArr = new int[arr.length];
        int i, j;
        int flag;
        int found;

        for (i = 0; i < tempArr.length; i++) {
            tempArr[i] = 0;
        }

        for (i = 0, found = 0; i < arr.length; i++) {
            for (j = 0, flag = 0; j < arr.length; j++) {
                if (arr[i] == arr[j] && i != j) {
                    flag++;
                }
            }

            if (flag >= 2) {
                for (j = 0, flag = 0; j < found; j++) {
                    if (tempArr[j] == arr[i]) {
                        flag = 1;
                    }
                }
                if (flag == 0) {
                    tempArr[found] = arr[i];
                    found++;
                }
            }
        }
        for (int t = 0; t < arr.length; t++) {
            for (int z = 0; z < arr.length; z++) {
                if (tempArr[t] == arr[z]) {
                    arr[z] = 0;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
        System.out.println(Arrays.toString(tempArr));
        return arr;
    }

    private static void printArrWithoutDuplications(int[] arr) {
        for (int i = 0; i < arr.length-1; i++) {
            if (arr[i] == arr[i+1]) {
                arr[i] = 0;
            }
        }
        System.out.println(Arrays.toString(arr));
    }
}
