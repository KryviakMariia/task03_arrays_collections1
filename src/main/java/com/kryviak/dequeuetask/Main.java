package com.kryviak.dequeuetask;

import java.util.ArrayDeque;
import java.util.Arrays;


public class Main {
    public static ArrayDeque<String> states = new ArrayDeque<>();

    public static void main(String[] args) {
//        states.addLast("Something one");
//        states.add("Second");
//        states.push("ttt");
//        states.addFirst("Hello");
//        states.addLast("gg one");
//
//        System.out.println("After pop: " + states.pop());
//        System.out.println(states);

        MyArrayList myArrayList = new MyArrayList();

        for (int i = 0; i < 9; i++) {
//            myArrayList.add("v");
            myArrayList.add(new StringGenerator().generateString());
        }
        myArrayList.add("s");
        myArrayList.add("Ey");
        myArrayList.removeNull();

        Arrays.sort(myArrayList.toArr());
        for (int i = 0; i < myArrayList.size(); i++) {
            System.out.println(myArrayList.get(i));
        }
        System.out.println(myArrayList.size());

        myArrayList.binarySearch(myArrayList.toArr(), 0, myArrayList.size(), "s");

    }
}
