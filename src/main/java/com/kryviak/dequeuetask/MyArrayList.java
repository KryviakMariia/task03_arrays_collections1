package com.kryviak.dequeuetask;

public class MyArrayList {
    private final int INIT_SIZE = 9;
    private Object[] array = new Object[INIT_SIZE];
    private int pointer = 0;

    public void add(Object item) {
        if (pointer == array.length - 1)
            resize(array.length + 1);
        array[pointer++] = item;
    }

    public Object get(int index) {
        return array[index];
    }

    public void remove(int index) {
        for (int i = index; i < pointer; i++)
            array[i] = array[i + 1];
        array[pointer] = null;
        pointer--;
        if (array.length > INIT_SIZE && pointer < array.length - 1)
            resize(array.length - 2);

    }

    public int size() {
        return pointer;
    }

    private void resize(int newLength) {
        Object[] newArray = new Object[newLength];
        System.arraycopy(array, 0, newArray, 0, pointer);
        array = newArray;
    }

    public Object[] toArr() {
        return array;
    }

    public void removeNull() {
        for (int i = 0; i < array.length; i++) {
            if (get(i) == null) {
                remove(i);
            }
        }
    }

    public void binarySearch(Object[] array11, int first, int last, String item) {
        int position;
        int comparisonCount = 1;

        position = (first + last) / 2;

        while ((array11[position] != item) && (first <= last)) {
            comparisonCount++;
            if (item.compareTo((String) array11[position]) < 0) {
                last = position - 1;
            } else {
                first = position + 1;
            }
            position = (first + last) / 2;
        }
        if (first <= last) {
            System.out.println(item + " is " + ++position + " array element.");
            System.out.println("Were performed " + comparisonCount + " comparisons");
        } else {
            System.out.println("Element was not found.");
        }
    }
}
