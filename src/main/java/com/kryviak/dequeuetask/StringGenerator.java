package com.kryviak.dequeuetask;

import java.util.Random;
import java.util.UUID;

public class StringGenerator {

    private static final String GENERATOR_PATTERN = "s;;;;;;;;;;;lkjhgfrfgbn m";

    public String generateString() {
        Random random = new Random();
        int rand = GENERATOR_PATTERN.length();
        StringBuffer sb = new StringBuffer();
        int length = random.nextInt(12);
        for (int i = 0; i < length; i++) {
            sb.append(GENERATOR_PATTERN.charAt(random.nextInt(rand)));
        }
        return sb.toString();
    }

    String randomString() {
        return UUID.randomUUID().toString();
    }
}
