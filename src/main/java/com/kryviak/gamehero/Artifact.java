package com.kryviak.gamehero;

public class Artifact {
    private int health;

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    @Override
    public String toString() {
        return "Artifact: [health = " + this.health + "]";
    }
}
