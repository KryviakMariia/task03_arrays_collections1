package com.kryviak.gamehero;

import java.util.Random;

public class Main {

    private static final int ROOMS_NUMBER = 10;
    private static final int START_HERO_POWER = 25;

    public static void main(String[] arg) {
        Hero hero = new Hero();
        hero.setPower(START_HERO_POWER);
        Main main = new Main();
        Rooms rooms = main.setRooms(ROOMS_NUMBER);

        System.out.println("Generated Rooms:");
        (rooms.getRooms()).forEach(System.out::println);

        main.printNumberOfDeath(rooms, hero);
        main.enterRooms(rooms, hero);

    }

    private Rooms setRooms(int roomsNumber) {
        Rooms rooms = new Rooms();
        for (int i = 0; i < roomsNumber; i++) {
            int temp = new Random().nextInt(2);
            if (temp == 0) {
                Monster monster = new Monster();
                monster.setPower(new Random().nextInt(100));
                rooms.setRooms(monster);
            } else {
                Artifact artifact = new Artifact();
                artifact.setHealth(new Random().nextInt(80));
                rooms.setRooms(artifact);
            }
        }
        return rooms;
    }

    private void printNumberOfDeath(Rooms rooms, Hero hero) {
        int counter = 0;
        for (int i = 0; i < rooms.getRooms().size(); i++) {
            if (rooms.getRooms().get(i) instanceof Monster) {
                if (((Monster) rooms.getRooms().get(i)).getPower() > hero.getPower()) {
                    counter++;
                }
            }
        }
        System.out.println("Number of death for hero = " + counter);
    }

    private void enterRooms(Rooms rooms, Hero hero) {
        for (int i = 0; i < rooms.getRooms().size(); i++) {
            if (rooms.getRooms().get(i) instanceof Artifact) {
                hero.setPower(hero.getPower() + ((Artifact) rooms.getRooms().get(i)).getHealth());
            } else if (rooms.getRooms().get(i) instanceof Monster) {
                if (((Monster) rooms.getRooms().get(i)).getPower() < hero.getPower() && hero.getPower() > 0) {
                    hero.setPower(hero.getPower() - ((Monster) rooms.getRooms().get(i)).getPower());
                } else {
                    System.out.println("Hero is dead");
                    break;
                }
            }
        }
    }
}

