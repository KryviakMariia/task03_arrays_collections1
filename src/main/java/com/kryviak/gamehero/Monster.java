package com.kryviak.gamehero;

public class Monster {
    private int power;

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return "Monster: [power = " + this.power + "]";
    }
}
