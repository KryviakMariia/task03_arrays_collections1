package com.kryviak.gamehero;

import java.util.ArrayList;
import java.util.List;

public class Rooms {
    private List<Object> rooms = new ArrayList<>();

    public List<Object> getRooms() {
        return rooms;
    }

    public void setRooms(Object room) {
        this.rooms.add(room);
    }
}
