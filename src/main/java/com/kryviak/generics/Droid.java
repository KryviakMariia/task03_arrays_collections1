package com.kryviak.generics;


public class Droid {
    private String name;
    private int power;
    private int helth;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPower() {
        return this.power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public int getHelth() {
        return this.helth;
    }

    public void setHelth(int helth) {
        this.helth = helth;
    }

    @Override
    public String toString() {
        return "Name: '" + this.name + "', Power: '" + this.power + "', Health: '" + this.helth + "'";
    }
}
