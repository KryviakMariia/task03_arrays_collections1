package com.kryviak.generics;

import java.util.*;

public class Main {
    public static void main(String[]args) {
        Main main = new Main();
        System.out.println(main.getParameters());

        Droid d1 = new Droid();
        d1.setName("First droid");
        d1.setPower(50);
        d1.setHelth(100);

        Droid d2 = new Droid();
        d2.setName("Second droid");
        d2.setPower(40);
        d2.setHelth(300);

        Droid d3 = new Droid();
        d3.setName("Third droid");
        d3.setPower(60);
        d3.setHelth(200);

        Droid d4 = new Droid();
        d4.setName("Forth droid");
        d4.setPower(55);
        d4.setHelth(200);
        Ship ship = new Ship();
        ship.setDroids(d1);
        ship.setDroids(d2);
        ship.setDroids(d3);
        ship.setDroids(d4);

        Iterator it = ship.getDroidQueue().iterator();

        System.out.println("Priority queue values are: ");

        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }

    private List<Integer> getParameters() {
        List<Integer> params = new ArrayList<>();
        params.add(Integer.valueOf("1973"));
        return params;
    }
}
