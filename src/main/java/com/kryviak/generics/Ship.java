package com.kryviak.generics;

import java.util.PriorityQueue;

public class Ship {

    private PriorityQueue<Droid> droidQueue = new PriorityQueue<>(
                        (a, b) -> a.getHelth() < b.getHelth() ? -1 : a.getPower() == b.getPower() ? 0 : 1);

    public void setDroids(Droid droid) {
        this.droidQueue.add(droid);
    }

    public PriorityQueue<Droid> getDroidQueue() {
        return this.droidQueue;
    }
}
